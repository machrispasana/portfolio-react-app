import { useSwiper } from 'swiper/react';

import { TiArrowForward } from 'react-icons/ti'

export default function SlideNextButton() {
  const swiper = useSwiper();

  return (
    <button className='btn-slide slideNextButton' onClick={() => swiper.slideNext()}>
    
        <TiArrowForward className='arrow-icon next'/>

    </button>
  );
}