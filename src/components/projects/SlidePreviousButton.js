import { useSwiper } from 'swiper/react';

import { TiArrowBack } from 'react-icons/ti';

export default function SlidePreviousButton() {
  const swiper = useSwiper();

  return (
    <button className='btn-slide slidePreviousButton' onClick={() => swiper.slidePrev()}>
      <TiArrowBack className='arrow-icon previous'/>
      
      
    </button>
  );
}