
import useMediaQuery from "../hooks/UseMediaQuery";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";


// import required modules
import { Pagination, Navigation } from "swiper";


import projectsData from "../../data/ProjectsData";
import SlideNextButton from './SlideNextButton';
import SlidePreviousButton from './SlidePreviousButton';
import Card  from './Card'

import './projects.css';



const Projects = () => {
  console.log(projectsData)
  const isLargeDisplay = useMediaQuery('(min-width: 1320px)');
  const pagination = {
    clickable: true,
    renderBullet: function (index, className) {
      return '<span class="' + className + '">' + (index + 1) + "</span>";
    },
  };

  let prevarrow = document.getElementsByClassName('slidePreviousButton')[0];
  let nextarrow = document.getElementsByClassName('slideNextButton')[0];

  return (
    <section id='projects'>

      <div className="container project-container">

        <div className="title_project">
          <h5>My Recent Work</h5>
          <h2>Projects</h2>
        </div>

        
          <div className="projects-wrapper">
                <Swiper
                  onSlideChange={(swiperCore) => {
                  const {
                    activeIndex,
                    snapIndex,
                    previousIndex,
                    realIndex,
                  } = swiperCore;
                  console.log({ activeIndex, snapIndex, previousIndex, realIndex })
                  
                  if (realIndex === 0) {
                    console.log("real index:" + realIndex + " - hide arrow");
                    prevarrow.style.display = 'none';
                    nextarrow.style.display = 'block';
                  } else {
                    console.log("real index:" + realIndex + " - show arrow");
                    prevarrow.style.display = 'block';
                    nextarrow.style.display = 'none';
                  }
                  }}
                  grabCursor={true}
                  slidesPerView={3}
                  spaceBetween={isLargeDisplay ? 40 : 0}
                  navigation={isLargeDisplay ? true : false}
                  direction={isLargeDisplay ? 'horizontal' : 'vertical'}
                  pagination={pagination}
                  modules={[Pagination, Navigation]}
                  className="mySwiperProjects"
              >
                {projectsData.map((data) => (
                    <SwiperSlide className="mySwiperslide" lazy="true">
                      <Card key={projectsData.id} data={data} loading="lazy"/>
                    </SwiperSlide>
                  
                  ))}
                 <SlideNextButton/>
                  <SlidePreviousButton/>

              </Swiper>

              

              
            
          </div>
        
        
        
      </div>
    </section>
  )
}

export default Projects