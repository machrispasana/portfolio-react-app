import Ecommerce from '../assets/projects/1.png'
import Portfolio from '../assets/projects/2.png'
import EcommerceAPI from '../assets/projects/3.png'
import PortfolioV2 from '../assets/projects/4.jpg'



const projectsData = [
    {
        id: 1,
        image: Portfolio,
        title: 'Portfolio Website',
        subtitle: 'Static responsive portfolio website hosted in Gitlab',
        git: 'https://github.com/machris24/webportfolio',
        demo: 'https://machris24.github.io/webportfolio/',
        tags: ["HTML", "CSS", "Bootstrap", "JavaScrpt", "Git"]
    },
    {
        id: 2,
        image: EcommerceAPI,
        title: 'Ecommerce API',
        subtitle: 'Developed an E-Commerce API using Node and Express.js',
        git: 'https://github.com/machris24/ecommerce-api',
        demo: 'https://documenter.getpostman.com/view/24219045/2s8Z6vYERh',
        tags: ["MongoDB", "Express.js", "REST API", "Node.js", "JSON", "Robo3T", "Postman", "Render", "Git"]
    },
    {
        id: 3,
        image: Ecommerce,
        title: 'Ecommerce Website',
        subtitle: 'Completing an E-commerce Full-Stack Application using the MERN Stack',
        git: 'https://github.com/machris24/ecommerce-website',
        demo: 'https://ecommerce-app-react-six.vercel.app/',
        tags: ["React.js", "JavaScript", "Bootstrap", "CSS", "HTML", "MongoDB", "Vercel"]
    },
    {
        id: 4,
        image: PortfolioV2,
        title: 'Portfolio V2.0',
        subtitle: 'Complete animated portfolio version 2.0 using React',
        git: 'https://github.com/machris24/react-portfolio',
        demo: 'https://portfolio-react-app-tawny.vercel.app/',
        tags: ["React.js", "JavaScript", "Bootstrap", "CSS", "HTML", "MongoDB", "Vercel"]
    }

]

export default projectsData;